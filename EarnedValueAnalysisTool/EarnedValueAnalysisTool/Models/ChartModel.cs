﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarnedValueAnalysisTool.Models
{
    public class ChartModel
    {
        public DateTime Date { get; set; }
        public float Value { get; set; }

        public ChartModel(DateTime date, float value)
        {
            Date = date;
            Value = value;
        }
    }
}
