﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarnedValueAnalysisTool.Importer
{
    public class ImporterFactory
    {
        private static ImporterFactory instance;
        public static ImporterFactory Instance
        {
            get
            {
                if (instance == null)
                    instance = new ImporterFactory();

                return instance;
            }
        }
        private ImporterFactory()
        {

        }

        public IImporter CreateImporter(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName);

            switch(fileExtension)
            {
                case ".xlsx":
                    return new ExcelImporter();
                default:
                    throw new NotImplementedException("Für diesen Dateitypen wurde noch kein Importer entwickelt!");
            }
        }
    }
}
