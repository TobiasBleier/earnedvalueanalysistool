﻿using EarnedValueAnalysisTool.ViewModels;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EarnedValueAnalysisTool.Importer
{
    public class ExcelImporter : IImporter
    {
        public ImportResult Import(string fileName)
        {
            DataSet excelDataSet = null;
            ImportResult result = null;

            try
            {
                using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        excelDataSet = reader.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            ConfigureDataTable = (o) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true
                            }
                        });
                    }
                }

                result = ConvertDataSetToImportResult(excelDataSet);
            }
            catch(Exception e)
            {
                MessageBox.Show("An Error ocurred!" + "\n" + e.Message);
                throw;
            }

            return result;
        }

        private ImportResult ConvertDataSetToImportResult(DataSet excelDataSet)
        {
            ImportResult result = new ImportResult();

            DataTable projectInformationTable = excelDataSet.Tables["ProjectInformation"];

            result.APNOfferNumber = projectInformationTable.Rows[0]["APN"].ToString();
            result.ProjectName = projectInformationTable.Rows[0]["ProjectName"].ToString();
            result.PlannedDuration = float.Parse(projectInformationTable.Rows[0]["PlannedDuration"].ToString());
            result.PlannedTotalCost = float.Parse(projectInformationTable.Rows[0]["PlannedTotalCost"].ToString());

            List<ProjectProgressSnapshotViewModel> progressSnapshots = GetProgressSnapshotsFromDataTable(excelDataSet.Tables["MileStones"], result.PlannedDuration, result.PlannedTotalCost);
            result.ProgressSnapshots = progressSnapshots;

            return result;
        }

        private List<ProjectProgressSnapshotViewModel> GetProgressSnapshotsFromDataTable(DataTable dataTable, float plannedDuration, float totalPlannedCosts)
        {
            List<ProjectProgressSnapshotViewModel> result = new List<ProjectProgressSnapshotViewModel>();

            foreach(DataRow row in dataTable.Rows)
            {
                ProjectProgressSnapshotViewModel projectProgressSnapshot = new ProjectProgressSnapshotViewModel();
                projectProgressSnapshot.PlannedTotalDuration = plannedDuration;
                projectProgressSnapshot.TotalPlannedCost = totalPlannedCosts;
                projectProgressSnapshot.SnapShotDate = DateTime.Parse(row["Date"].ToString(),CultureInfo.CurrentCulture);
                projectProgressSnapshot.SnapShotPlannedValue = float.Parse(row["PlannedCosts"].ToString());
                projectProgressSnapshot.ActualCost = float.Parse(row["ActualCosts"].ToString());
                projectProgressSnapshot.PercentComplete = float.Parse(row["PercentComplete"].ToString());

                result.Add(projectProgressSnapshot);
            }

            return result;
        }
    }
}
