﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarnedValueAnalysisTool.Importer
{
    public class ImportResult
    {
        public string ProjectName { get; set; }
        public string APNOfferNumber { get; set; }
        public float PlannedDuration { get; set; }
        public float PlannedTotalCost { get; set; }
        public IEnumerable<ProjectProgressSnapshotViewModel> ProgressSnapshots { get; set; }
    }
}
