﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EarnedValueAnalysisTool.Views
{
    public class DependencyWrapper : DependencyObject
    {
        public static readonly DependencyProperty FormatProperty =
             DependencyProperty.Register("ValidatedCollection", typeof(ObservableCollection<ProjectProgressSnapshotViewModel>),
             typeof(DependencyWrapper), new FrameworkPropertyMetadata(null));

        public ObservableCollection<ProjectProgressSnapshotViewModel> ValidatedCollection
        {
            get { return (ObservableCollection<ProjectProgressSnapshotViewModel>)GetValue(FormatProperty); }
            set { SetValue(FormatProperty, value); }
        }
    }
}
