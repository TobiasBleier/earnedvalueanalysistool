﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace EarnedValueAnalysisTool.Views
{
    public class UniqueSnapShotDateRule : ValidationRule
    {
        public DependencyWrapper DependencyWrapper { get; set; }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {

            if (value != null && value is DateTime)
            {
                var validatedSnapshotTime = (DateTime)value;
                var validatedCollection = DependencyWrapper.ValidatedCollection;

                if(validatedCollection.FirstOrDefault(projectProgressSnapshot => projectProgressSnapshot.SnapShotDate == validatedSnapshotTime) != null)
                {
                    return new ValidationResult(false, value);
                }
            }

            return new ValidationResult(true, null);
        }
    }
}
