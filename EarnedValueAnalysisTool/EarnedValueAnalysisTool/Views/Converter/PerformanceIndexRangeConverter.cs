﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace EarnedValueAnalysisTool.Views.Converter
{
    public class PerformanceIndexRangeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && float.TryParse(value.ToString(),NumberStyles.Any,CultureInfo.InvariantCulture, out float performanceIndex))
            {
                if (performanceIndex >= 1)
                {
                    return true;
                }
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
