﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace EarnedValueAnalysisTool
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            ComposeObjects();

            MainWindow.ShowDialog();
        }

        private static void ComposeObjects()
        {
            var projectViewModel = new ProjectViewModel();
            var chartViewModel = new ChartViewModel();

            var viewModel = new MainViewModel(projectViewModel, chartViewModel);

            MainWindow mainWindow = new MainWindow();
            mainWindow.DataContext = viewModel;

            Application.Current.MainWindow = mainWindow;
        }
    }
}
