﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EarnedValueAnalysisTool.Commands
{
    public class ImportCommand : ICommand
    {
        private readonly MainViewModel mainViewModel;
        public event EventHandler CanExecuteChanged;

        public ImportCommand(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            mainViewModel.Import();
        }
    }
}
