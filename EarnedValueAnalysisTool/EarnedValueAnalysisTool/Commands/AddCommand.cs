﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EarnedValueAnalysisTool.Commands
{
    internal class AddCommand : ICommand
    {
        private readonly ProjectViewModel projectViewModel;
        public event EventHandler CanExecuteChanged;

        public AddCommand(ProjectViewModel projectViewModel)
        {
            this.projectViewModel = projectViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            projectViewModel.AddProjectProgressSnapshot();
        }
    }
}
