﻿using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EarnedValueAnalysisTool.Commands
{
    class RefreshChartCommand : ICommand
    {
        private readonly MainViewModel mainViewModel;
        public event EventHandler CanExecuteChanged;

        public RefreshChartCommand(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            mainViewModel.RefreshChartValues();
        }
    }
}
