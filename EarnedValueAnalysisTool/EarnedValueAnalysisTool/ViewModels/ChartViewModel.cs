﻿using EarnedValueAnalysisTool.Models;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Helpers;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarnedValueAnalysisTool.ViewModels
{
    public class ChartViewModel : ViewModelBase
    {
        public SeriesCollection SeriesCollection { get; set; }
        public Func<double, string> XFormatter { get; set; }

        public ChartViewModel()
        {
            var personDayConfig = Mappers.Xy<ChartModel>()
                                    .X(chartModel => chartModel.Date.Ticks)
                                    .Y(chartModel => chartModel.Value);

            SeriesCollection = new SeriesCollection(personDayConfig)
            {
                new LineSeries
                {
                    Title = "Tatsächliche Kosten",
                    Values = new ChartValues<ChartModel>(),
                },
                new LineSeries
                {
                    Title = "Geplante Kosten",
                    Values = new ChartValues<ChartModel>()
                },
                new LineSeries
                {
                    Title = "Fertigstellungswert",
                    Values = new ChartValues<ChartModel>()
                },
            };

            XFormatter = (value => new DateTime((long)value).ToString("dd.MM.yyyy"));
        }

        public void RefreshChartValues(List<ChartModel> actualCosts, List<ChartModel> plannedCosts, List<ChartModel> earnedValue)
        {
            SeriesCollection[0].Values = actualCosts.AsChartValues();
            SeriesCollection[1].Values = plannedCosts.AsChartValues();
            SeriesCollection[2].Values = earnedValue.AsChartValues();
        }
    }
}
