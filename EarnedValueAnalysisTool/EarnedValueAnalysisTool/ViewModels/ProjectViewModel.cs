﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using EarnedValueAnalysisTool.Commands;
using EarnedValueAnalysisTool.Models;
using LiveCharts;
using LiveCharts.Helpers;
using LiveCharts.Wpf;

namespace EarnedValueAnalysisTool.ViewModels
{
    public class ProjectViewModel : ViewModelBase
    {

        private string projectName;
        public string ProjectName
        {
            get
            {
                return projectName;
            }
            set
            {
                projectName = value;
                OnPropertyChanged();
            }
        }

        private string apnOfferNumber;
        public string APNOfferNumber
        {
            get
            {
                return apnOfferNumber;
            }
            set
            {
                apnOfferNumber = value;
                OnPropertyChanged();
            }
        }

        private float plannedTotalDuration;
        public float PlannedTotalDuration
        {
            get
            {
                return plannedTotalDuration;
            }
            set
            {
                plannedTotalDuration = value;
                RecalculatePlannedTotalDurationInProgressSnapshots();
                OnPropertyChanged();
            }
        }

        private float plannedTotalCost;
        public float PlannedTotalCost
        {
            get
            {
                return plannedTotalCost;
            }
            set
            {
                plannedTotalCost = value;
                RecalculateTotalPlannedCostInProgressSnapshots();
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ProjectProgressSnapshotViewModel> progressSnapshots;
        public ObservableCollection<ProjectProgressSnapshotViewModel> ProgressSnapshots
        {
            get
            {
                return progressSnapshots;
            }
            set
            {
                progressSnapshots = value;
                OnPropertyChanged();
            }
        }


        private ProjectProgressSnapshotViewModel selectedSnapshot;
        public ProjectProgressSnapshotViewModel SelectedSnapshot {
            get
            {
                return selectedSnapshot;
            }
            set
            {
                selectedSnapshot = value;
                OnPropertyChanged();
            }
        }



        #region COMMANDS

        public ICommand AddCommand{ get; set; }
        public ICommand DeleteCommand { get; set; }


        #endregion

        public ProjectViewModel()
        {

            ProgressSnapshots = new ObservableCollection<ProjectProgressSnapshotViewModel>();

            AddCommand = new AddCommand(this);
            DeleteCommand = new DeleteCommand(this);
        }

        private void RecalculatePlannedTotalDurationInProgressSnapshots()
        {
            foreach (var snapshot in ProgressSnapshots)
            {
                snapshot.PlannedTotalDuration = PlannedTotalDuration;
            }
        }

        private void RecalculateTotalPlannedCostInProgressSnapshots()
        {
            foreach (var snapshot in ProgressSnapshots)
            {
                snapshot.TotalPlannedCost = PlannedTotalCost;
            }
        }

        internal void AddProjectProgressSnapshot()
        {
            DateTime snapShotDate = DateTime.Today;

            if (ProgressSnapshots.Count != 0)
            {
                DateTime highestSnapShotDate = ProgressSnapshots.Max(progressSnapshot => progressSnapshot.SnapShotDate);
                snapShotDate = highestSnapShotDate.AddDays(1);
            }
            
            ProgressSnapshots.Add(new ProjectProgressSnapshotViewModel(snapShotDate,PlannedTotalCost,0, PlannedTotalDuration));
        }
        internal void RemoveProjectProgressSnapshot(ProjectProgressSnapshotViewModel progressSnapshot)
        {
            ProgressSnapshots.Remove(progressSnapshot);
        }

    }
}
