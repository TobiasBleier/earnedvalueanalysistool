﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarnedValueAnalysisTool.ViewModels
{
    public class ProjectProgressSnapshotViewModel : ViewModelBase
    {
        private DateTime snapShotDate;
        public DateTime SnapShotDate {
            get
            {
                return snapShotDate;
            }
            set
            {
                snapShotDate = value;
                OnPropertyChanged();
            }
        }

        private float actualCost;
        public float ActualCost
        {
            get
            {
                return actualCost;
            }
            set
            {
                actualCost = value;
                CalculateCostPerformanceIndex();
                OnPropertyChanged();
            }
        }

        private float totalPlannedCost;
        public float TotalPlannedCost {
            get
            {
                return totalPlannedCost;
            }
            set
            {
                totalPlannedCost = value;
                CalculateEarnedValue();
                CalculateEstimatedAtCompletion();
                OnPropertyChanged();
            }
        }

        private float snapShotPlannedValue;
        public float SnapShotPlannedValue
        {
            get
            {
                return snapShotPlannedValue;
            }
            set
            {
                snapShotPlannedValue = value;
                CalculateScheduledPerformanceIndex();
            }
        }


        private float percentComplete;
        public float PercentComplete
        {
            get
            {
                return percentComplete;
            }
            set
            {
                percentComplete = value;
                CalculateEarnedValue();
                OnPropertyChanged();
            }
        }

        private float earnedValue;
        public float EarnedValue
        {
            get
            {
                return earnedValue;
            }
            private set
            {
                earnedValue = value;
                CalculateScheduledPerformanceIndex();
                CalculateCostPerformanceIndex();
                OnPropertyChanged();
            }
        }

        private float estimatedTotalCost;
        public float EstimatedTotalCost
        {
            get
            {
                return estimatedTotalCost;
            }
            set
            {
                estimatedTotalCost = value;
                OnPropertyChanged();
            }
        }

        private float scheduledPerformanceIndex;
        public float ScheduledPerformanceIndex
        {
            get
            {
                return scheduledPerformanceIndex;
            }
            set
            {
                scheduledPerformanceIndex = value;
                CalculateEstimatedTotalDuration();
                OnPropertyChanged();
            }
        }

        private float estimatedTotalDuration;
        public float EstimatedTotalDuration
        {
            get
            {
                return estimatedTotalDuration;
            }
            private set
            {
                estimatedTotalDuration = value;
                OnPropertyChanged();
            }
        }

        private float plannedTotalDuration;
        public float PlannedTotalDuration
        {
            get
            {
                return plannedTotalDuration;
            }
            set
            {
                plannedTotalDuration = value;
                CalculateEstimatedTotalDuration();
            }
        }

        private float costPerformanceIndex;
        public float CostPerformanceIndex
        {
            get
            {
                return costPerformanceIndex;
            }

            private set
            {
                costPerformanceIndex = value;
                CalculateEstimatedAtCompletion();
                OnPropertyChanged();
            }
        }

        #region Constructor
        public ProjectProgressSnapshotViewModel()
        {
            //Empty Constructor to make tests easier
        }

        public ProjectProgressSnapshotViewModel(DateTime dateTime, float _plannedTotalCost, float _percentComplete, float _plannedTotalDuration)
        {
            SnapShotDate = dateTime;
            totalPlannedCost = _plannedTotalCost;
            percentComplete = _percentComplete;
            plannedTotalDuration = _plannedTotalDuration;
        }

        #endregion

        #region Methods

        private void CalculateEarnedValue()
        {
            EarnedValue = TotalPlannedCost * (PercentComplete / 100);
        }

        private void CalculateScheduledPerformanceIndex()
        {
            if (SnapShotPlannedValue > 0)
                ScheduledPerformanceIndex = EarnedValue / SnapShotPlannedValue;
        }

        private void CalculateEstimatedTotalDuration()
        {
            if (ScheduledPerformanceIndex > 0)
                EstimatedTotalDuration = PlannedTotalDuration / ScheduledPerformanceIndex;
        }

        private void CalculateCostPerformanceIndex()
        {
            if (ActualCost > 0)
                CostPerformanceIndex = EarnedValue / ActualCost;
        }

        private void CalculateEstimatedAtCompletion()
        {
            if (CostPerformanceIndex > 0)
                EstimatedTotalCost = TotalPlannedCost / CostPerformanceIndex;
        }

        #endregion
    }
}
