﻿using EarnedValueAnalysisTool.Commands;
using EarnedValueAnalysisTool.Importer;
using EarnedValueAnalysisTool.Models;
using EarnedValueAnalysisTool.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;

namespace EarnedValueAnalysisTool.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public ProjectViewModel ProjectViewModel { get; private set; }
        public ChartViewModel ChartViewModel { get; private set; }

        public ICommand RefreshChartCommand { get; set; } 
        public ICommand ImportCommand { get; set; }

        public MainViewModel(ProjectViewModel projectViewModel, ChartViewModel chartViewModel)
        {
            ProjectViewModel = projectViewModel;
            ChartViewModel = chartViewModel;

            RefreshChartCommand = new RefreshChartCommand(this);
            ImportCommand = new ImportCommand(this);
        }

        internal void RefreshChartValues()
        {
            List<ProjectProgressSnapshotViewModel> sortedProgressSnapshots = ProjectViewModel.ProgressSnapshots.OrderBy(snapshot => snapshot.SnapShotDate).ToList();
            if(ProjectViewModel.SelectedSnapshot != null)
            {
                DateTime selectedSnapshotDate = ProjectViewModel.SelectedSnapshot.SnapShotDate;
                List<ChartModel> plannedCosts = ChartModelConverter.GetPlannedCostChartModelsFromSnapshots(sortedProgressSnapshots);
                List<ChartModel> actualCosts = ChartModelConverter.GetActualCostChartModelsFromSnapshots(sortedProgressSnapshots, selectedSnapshotDate);
                List<ChartModel> earnedValue = ChartModelConverter.GetEarnedValueChartModelsFromSnapshots(sortedProgressSnapshots, selectedSnapshotDate);

                ChartViewModel.RefreshChartValues(actualCosts, plannedCosts, earnedValue);
            }
            else
            {
                MessageBox.Show("Kein Stichtag ausgewählt!","Warnung");
            }

        }

        internal void Import()
        {
            var fileDialog = new OpenFileDialog()
            {
                FileName = "Bitte wählen Sie eine Datei",
                Filter = "Excel 2007 Datei (*.xlsx)|*.xlsx",
                Title = "Öffnen einer Excel-Datei"
            };


            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                IImporter importer = ImporterFactory.Instance.CreateImporter(fileDialog.FileName);
                ImportResult result = importer.Import(fileDialog.FileName);

                ProjectViewModel.APNOfferNumber = result.APNOfferNumber;
                ProjectViewModel.ProjectName = result.ProjectName;
                ProjectViewModel.PlannedTotalCost = result.PlannedTotalCost;
                ProjectViewModel.PlannedTotalDuration = result.PlannedDuration;

                var resultProgressSnapshots = new ObservableCollection<ProjectProgressSnapshotViewModel>(result.ProgressSnapshots);
                ProjectViewModel.ProgressSnapshots = resultProgressSnapshots;
            }
        }
    }
}
