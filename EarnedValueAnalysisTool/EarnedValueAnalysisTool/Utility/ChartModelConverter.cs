﻿using EarnedValueAnalysisTool.Models;
using EarnedValueAnalysisTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarnedValueAnalysisTool.Utility
{
    public class ChartModelConverter
    {
        public static List<ChartModel> GetPlannedCostChartModelsFromSnapshots(List<ProjectProgressSnapshotViewModel> progressSnapshots)
        {
            List<ChartModel> plannedCostChartModels = new List<ChartModel>();

            foreach (var snapshot in progressSnapshots)
            {
                ChartModel chartModel = new ChartModel(snapshot.SnapShotDate, snapshot.SnapShotPlannedValue);

                plannedCostChartModels.Add(chartModel);
            }

            return plannedCostChartModels;
        }

        public static List<ChartModel> GetActualCostChartModelsFromSnapshots(List<ProjectProgressSnapshotViewModel> progressSnapshots, DateTime selectedSnapshotDate)
        {
            List<ChartModel> actualCostChartModels = new List<ChartModel>();

            foreach (var snapshot in progressSnapshots.Where(snapshot => snapshot.SnapShotDate <= selectedSnapshotDate))
            {
                ChartModel chartModel = new ChartModel(snapshot.SnapShotDate, snapshot.ActualCost);

                actualCostChartModels.Add(chartModel);
            }

            return actualCostChartModels;
        }

        public static List<ChartModel> GetEarnedValueChartModelsFromSnapshots(List<ProjectProgressSnapshotViewModel> progressSnapshots, DateTime selectedSnapshotDate)
        {
            List<ChartModel> earnedValueChartModels = new List<ChartModel>();

            foreach (var snapshot in progressSnapshots.Where(snapshot => snapshot.SnapShotDate <= selectedSnapshotDate))
            {
                ChartModel chartModel = new ChartModel(snapshot.SnapShotDate, snapshot.EarnedValue);

                earnedValueChartModels.Add(chartModel);
            }

            return earnedValueChartModels;
        }
    }
}
