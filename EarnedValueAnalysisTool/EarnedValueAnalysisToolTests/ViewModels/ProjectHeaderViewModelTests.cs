﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EarnedValueAnalysisTool.ViewModels;
using FluentAssertions;

namespace EarnedValueAnalysisToolTests
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für ProjectHeaderViewModelTests
    /// </summary>
    [TestClass]
    public class ProjectViewModelTests
    {
        public ProjectViewModelTests()
        {
            //
            // TODO: Konstruktorlogik hier hinzufügen
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Ruft den Textkontext mit Informationen über
        ///den aktuellen Testlauf sowie Funktionalität für diesen auf oder legt diese fest.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Zusätzliche Testattribute
        //
        // Sie können beim Schreiben der Tests folgende zusätzliche Attribute verwenden:
        //
        // Verwenden Sie ClassInitialize, um vor Ausführung des ersten Tests in der Klasse Code auszuführen.
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Verwenden Sie ClassCleanup, um nach Ausführung aller Tests in einer Klasse Code auszuführen.
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Mit TestCleanup können Sie nach jedem Test Code ausführen.
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ExecuteAddCommand_ShouldAddProjectProgressSnapshotViewModel()
        {
            //Arrange
            ProjectViewModel projectViewModel = new ProjectViewModel();

            //Act
            projectViewModel.AddCommand.Execute(null);

            //Assert
            projectViewModel.ProgressSnapshots.Should().HaveCount(1);
        }

        [TestMethod]
        public void ExecuteDeleteCommand_ShouldDeleteProjectProgressSnapshotViewModel()
        {
            //Arrange
            ProjectViewModel projectViewModel = new ProjectViewModel();
            ProjectProgressSnapshotViewModel projectProgressSnapshot = new ProjectProgressSnapshotViewModel();

            projectViewModel.ProgressSnapshots.Add(projectProgressSnapshot);

            //Act
            projectViewModel.DeleteCommand.Execute(projectProgressSnapshot);

            //Assert
            projectViewModel.ProgressSnapshots.Should().HaveCount(0);
        }
    }
}
