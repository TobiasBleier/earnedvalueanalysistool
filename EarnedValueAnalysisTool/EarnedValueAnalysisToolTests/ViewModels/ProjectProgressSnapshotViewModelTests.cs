﻿using System;
using EarnedValueAnalysisTool.ViewModels;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EarnedValueAnalysisToolTests
{
    [TestClass]
    public class ProjectProgressSnapshotViewModelTests
    {

        public ProjectProgressSnapshotViewModelTests()
        {
        }

        [TestMethod]
        public void SetPlannedValue_ShouldCalculateEarnedValue()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Act
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;

            //Assert
            projectProgressSnapshotViewModel.EarnedValue.Should().Be(3);
        }

        [TestMethod]
        public void SetPercentCompleted_ShouldCalculateEarnedValue()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;

            //Act
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Assert
            projectProgressSnapshotViewModel.EarnedValue.Should().Be(3);
        }

        [TestMethod]
        public void SetActualCost_ShouldCalculateCostPerformanceIndex()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Act
            projectProgressSnapshotViewModel.ActualCost = 3;

            //Assert
            projectProgressSnapshotViewModel.CostPerformanceIndex.Should().Be(1);
        }

        [TestMethod]
        public void SetEarnedValue_ShouldCalculateCostPerformanceIndex()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;
            projectProgressSnapshotViewModel.ActualCost = 3;


            //Act - Trigger EarnedValue Calculation trough Setter of PercentComplete
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Assert
            projectProgressSnapshotViewModel.CostPerformanceIndex.Should().Be(1);
        }

        [TestMethod]
        public void SetEarnedValue_ShouldCalculateSchedulePerformanceIndex()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;
            projectProgressSnapshotViewModel.SnapShotPlannedValue = 3;


            //Act - Trigger EarnedValue Calculation trough Setter of PercentComplete
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Assert
            projectProgressSnapshotViewModel.ScheduledPerformanceIndex.Should().Be(1);
        }

        [TestMethod]
        public void SetSnapShotPlannedValue_ShouldCalculateSchedulePerformanceIndex()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Act
            projectProgressSnapshotViewModel.SnapShotPlannedValue = 3;

            //Assert
            projectProgressSnapshotViewModel.ScheduledPerformanceIndex.Should().Be(1);
        }

        [TestMethod]
        public void SetCostPerformanceIndex_ShouldCalculateEstimatedTotalCost()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Act
            projectProgressSnapshotViewModel.ActualCost = 3;

            //Assert
            projectProgressSnapshotViewModel.EstimatedTotalCost.Should().Be(30, "we were in budget because of a CostPerformanceIndex of 1 (-> due to ActualCost = EarnedValue");
        }

        [TestMethod]
        public void SetTotalPlannedCost_ShouldCalculateEstimatedTotalCost()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.ActualCost = 3;
            projectProgressSnapshotViewModel.PercentComplete = 10;

            //Act
            projectProgressSnapshotViewModel.TotalPlannedCost = 30;

            //Assert
            projectProgressSnapshotViewModel.EstimatedTotalCost.Should().Be(30, "we were in budget because of a CostPerformanceIndex of 1 (-> due to ActualCost = EarnedValue");
        }

        [TestMethod]
        public void SetTotalPlannedDuration_ShouldCalculateEstimatedTotalDuration()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();

            //calculate a SPI of 1
            //EarnedValue of 10
            projectProgressSnapshotViewModel.TotalPlannedCost = 100;
            projectProgressSnapshotViewModel.PercentComplete = 10;
            projectProgressSnapshotViewModel.SnapShotPlannedValue = 10;

            //Act
            projectProgressSnapshotViewModel.PlannedTotalDuration = 100;

            //Assert
            projectProgressSnapshotViewModel.EstimatedTotalDuration.Should().Be(100, "we were in budget because of a ScheduledPerformanceIndex of 1 (-> due to SnapShotPlannedValue = EarnedValue");
        }

        [TestMethod]
        public void SetScheduledPerformanceIndex_ShouldCalculateEstimatedTotalDuration()
        {
            //Arrange
            ProjectProgressSnapshotViewModel projectProgressSnapshotViewModel = new ProjectProgressSnapshotViewModel();
            projectProgressSnapshotViewModel.PlannedTotalDuration = 100;

            //Act
            //calculate a SPI of 1
            //EarnedValue of 10
            projectProgressSnapshotViewModel.TotalPlannedCost = 100;
            projectProgressSnapshotViewModel.PercentComplete = 10;
            projectProgressSnapshotViewModel.SnapShotPlannedValue = 10;

            //Assert
            projectProgressSnapshotViewModel.EstimatedTotalDuration.Should().Be(100, "we were in budget because of a ScheduledPerformanceIndex of 1 (-> due to SnapShotPlannedValue = EarnedValue");
        }

    }
}
