﻿using System;
using EarnedValueAnalysisTool.Importer;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EarnedValueAnalysisToolTests.Importer
{
    [TestClass]
    public class ImporterFactoryTests
    {
        [TestMethod]
        public void CreateImporter_WithXLSX_File_ShouldReturnExcelImporter()
        {
            //Arrange

            //Act
            IImporter actual = ImporterFactory.Instance.CreateImporter("test.xlsx");

            //Assert
            actual.Should().BeOfType(typeof(ExcelImporter));
        }
    }
}
