﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EarnedValueAnalysisTool.Importer;
using EarnedValueAnalysisTool.ViewModels;
using System.Collections.Generic;
using FluentAssertions;

namespace EarnedValueAnalysisToolTests.Importer
{
    [TestClass]
    public class ExcelImporterTests
    {
        private const string TESTFILENAME = "ExcelImport.xlsx";

        [TestMethod]
        public void Import_WithCorrectFileName_ReturnsExpectedExcelResult()
        {
            //C:\Repositories\earnedvalueanalysistool\EarnedValueAnalysisTool\EarnedValueAnalysisToolTests\Importer\Artifacts
            //Arrange
            string testFilename = GetArtifactPath(TESTFILENAME);
            ExcelImporter excelImporter = new ExcelImporter();
            ImportResult expected = GetExpectedImportResult();

            //Act
            ImportResult actual = excelImporter.Import(testFilename);

            //Assert
            actual.Should().BeEquivalentTo(expected); 
        }

        private ImportResult GetExpectedImportResult()
        {
            float totalPlannedCost = 100;
            float totalPlannedDuration = 200;

            List<ProjectProgressSnapshotViewModel> progressSnapshotList = new List<ProjectProgressSnapshotViewModel>();

            ProjectProgressSnapshotViewModel mileStoneOne = new ProjectProgressSnapshotViewModel();
            mileStoneOne.SnapShotDate = new DateTime(2019, 1, 1);
            mileStoneOne.TotalPlannedCost = totalPlannedCost;
            mileStoneOne.PlannedTotalDuration = totalPlannedDuration;
            mileStoneOne.SnapShotPlannedValue = 20;
            mileStoneOne.ActualCost = 20;
            mileStoneOne.PercentComplete = 20;

            progressSnapshotList.Add(mileStoneOne);

            ProjectProgressSnapshotViewModel mileStoneTwo = new ProjectProgressSnapshotViewModel();
            mileStoneTwo.SnapShotDate = new DateTime(2019, 2, 1);
            mileStoneTwo.TotalPlannedCost = totalPlannedCost;
            mileStoneTwo.PlannedTotalDuration = totalPlannedDuration;
            mileStoneTwo.SnapShotPlannedValue = 40;
            mileStoneTwo.ActualCost = 40;
            mileStoneTwo.PercentComplete = 40;

            progressSnapshotList.Add(mileStoneTwo);

            ProjectProgressSnapshotViewModel mileStoneThree = new ProjectProgressSnapshotViewModel();
            mileStoneThree.SnapShotDate = new DateTime(2019, 3, 1);
            mileStoneThree.TotalPlannedCost = totalPlannedCost;
            mileStoneThree.PlannedTotalDuration = totalPlannedDuration;
            mileStoneThree.SnapShotPlannedValue = 60;
            mileStoneThree.ActualCost = 60;
            mileStoneThree.PercentComplete = 60;

            progressSnapshotList.Add(mileStoneThree);

            ProjectProgressSnapshotViewModel mileStoneFour = new ProjectProgressSnapshotViewModel();
            mileStoneFour.SnapShotDate = new DateTime(2019, 4, 1);
            mileStoneFour.TotalPlannedCost = totalPlannedCost;
            mileStoneFour.PlannedTotalDuration = totalPlannedDuration;
            mileStoneFour.SnapShotPlannedValue = 80;
            mileStoneFour.ActualCost = 80;
            mileStoneFour.PercentComplete = 80;

            progressSnapshotList.Add(mileStoneFour);

            ProjectProgressSnapshotViewModel mileStoneFive = new ProjectProgressSnapshotViewModel();
            mileStoneFive.SnapShotDate = new DateTime(2019, 5, 1);
            mileStoneFive.TotalPlannedCost = totalPlannedCost;
            mileStoneFive.PlannedTotalDuration = totalPlannedDuration;
            mileStoneFive.SnapShotPlannedValue = 100;
            mileStoneFive.ActualCost = 100;
            mileStoneFive.PercentComplete = 100;

            progressSnapshotList.Add(mileStoneFive);


            ImportResult expectedResult = new ImportResult
            {
                ProjectName = "TestProject",
                APNOfferNumber = "12345",
                PlannedDuration = totalPlannedDuration,
                PlannedTotalCost = totalPlannedCost,
                ProgressSnapshots = progressSnapshotList
            };

            return expectedResult;
        }

        private string GetArtifactPath(string filename)
        {
            //Go up 2 Folder to base TestProject directory
            string executingDirectory = Path.GetFullPath(@"..\..\");

            string artifactPath = Path.Combine(executingDirectory,"Importer","Artifacts",filename);

            return artifactPath;
        }
    }
}
