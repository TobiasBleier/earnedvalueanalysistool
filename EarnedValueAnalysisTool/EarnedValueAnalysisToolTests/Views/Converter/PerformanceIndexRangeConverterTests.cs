﻿using System;
using System.Globalization;
using EarnedValueAnalysisTool.Views.Converter;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EarnedValueAnalysisToolTests.Views.Converter
{
    [TestClass]
    public class PerformanceIndexRangeConverterTests
    {
        [TestMethod]
        public void Convert_ValueGreater1_ShouldReturnTrue()
        {
            //Arrange
            PerformanceIndexRangeConverter converter = new PerformanceIndexRangeConverter();

            //Act
            bool result = (bool)converter.Convert("2", null, null, CultureInfo.CurrentCulture);

            //Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void Convert_ValueOf1_ShouldReturnTrue()
        {
            //Arrange
            PerformanceIndexRangeConverter converter = new PerformanceIndexRangeConverter();

            //Act
            bool result = (bool)converter.Convert("1", null, null, CultureInfo.CurrentCulture);

            //Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void Convert_ValueSmaller1_ShouldReturnFalse()
        {
            //Arrange
            PerformanceIndexRangeConverter converter = new PerformanceIndexRangeConverter();

            //Act
            bool result = (bool)converter.Convert("0.5", null, null, CultureInfo.CurrentCulture);

            //Assert
            result.Should().BeFalse();
        }
    }
}
