﻿using EarnedValueAnalysisTool.ViewModels;
using EarnedValueAnalysisTool.Views;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace EarnedValueAnalysisToolTests.Views.ValidationRules
{
    [TestClass]
    public class UniqueSnapShotDateRuleTests
    {
        [TestMethod]
        public void Validate_WithNullValue_ShouldReturnPositiveValidationResult()
        {
            //Arrange
            var uniqueSnapShotDateRule = new UniqueSnapShotDateRule();
            var expected = new ValidationResult(true, null);

            //Act
            var result = uniqueSnapShotDateRule.Validate(null, CultureInfo.CurrentCulture);

            //Assert
            result.IsValid.Should().Be(expected.IsValid,"if a null value is validated there should be no negative ValidationResult because there should not be a validation at all");
        }

        [TestMethod]
        public void Validate_AlreadyExistingDateInCollection_ShouldReturnNegativeValidationResult()
        {
            //Arrange
            var uniqueSnapShotDateRule = new UniqueSnapShotDateRule();
            var dependencyWrapper = new DependencyWrapper();
            var collectionUsedForValidation = new ObservableCollection<ProjectProgressSnapshotViewModel> { new ProjectProgressSnapshotViewModel(new DateTime(1, 1, 1),250,10,200) };
            dependencyWrapper.ValidatedCollection = collectionUsedForValidation;
            uniqueSnapShotDateRule.DependencyWrapper = dependencyWrapper;

            var validatedSnapShotDate = new DateTime(1,1,1);

            
            var expected = new ValidationResult(false, validatedSnapShotDate);

            //Act
            var result = uniqueSnapShotDateRule.Validate(validatedSnapShotDate, CultureInfo.CurrentCulture);

            //Assert
            result.IsValid.Should().Be(expected.IsValid, "there is already a ProjectProgressSnapshotViewModel with the same SnapShotDate as the validated one");
        }
    }
}
